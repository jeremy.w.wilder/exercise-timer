package com.jwilder.exercise.timer.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jwilder.exercise.timer.utils.flowTimer
import com.jwilder.exercise.timer.utils.formatTime
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@InternalCoroutinesApi
class DelayTimerViewModel : ViewModel() {
    // TODO: Implement the ViewModel
    private val _timerCount = MutableLiveData<String>()
    val timerCount: LiveData<String>
        get() = _timerCount

    @ExperimentalCoroutinesApi
    fun startTimer() {
        viewModelScope.launch {
            flowTimer(25).collectLatest {
                _timerCount.value = formatTime(it)
            }
        }
    }
}
