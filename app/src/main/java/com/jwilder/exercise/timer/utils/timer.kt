package com.jwilder.exercise.timer.utils

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

fun flowTimer(timeSeconds: Int): Flow<Int> = flow {
    for (i in timeSeconds downTo 0) {
        delay(1_000)
        emit(i)
    }
}