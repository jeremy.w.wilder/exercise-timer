package com.jwilder.exercise.timer.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.jwilder.exercise.timer.R
import com.jwilder.exercise.timer.viewmodels.DelayTimerViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

class StopWatchFragment : Fragment() {

    @InternalCoroutinesApi
    private lateinit var viewModel: DelayTimerViewModel

    private var stopWatchTextView: TextView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.delay_timer_fragment, container, false)
    }

    @ExperimentalCoroutinesApi
    @InternalCoroutinesApi
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(DelayTimerViewModel::class.java)
        stopWatchTextView = view?.findViewById(R.id.stopWatchText)

        viewModel.timerCount.observe(viewLifecycleOwner, Observer {
            stopWatchTextView?.text = it
        })

        view?.findViewById<View>(R.id.fab)?.setOnClickListener {
            viewModel.startTimer()
        }
    }

    companion object {
        val TAG = StopWatchFragment::class.java.simpleName
        fun newInstance() = StopWatchFragment()
    }
}
