package com.jwilder.exercise.timer.utils

import kotlin.math.floor

fun formatTime(timeSeconds: Int): String {
    return when {
        timeSeconds < 10 -> "00:0$timeSeconds"
        timeSeconds > 60 -> {
            val m = floor(timeSeconds / 60.0).toInt().formatSection()
            val s = floor(timeSeconds % 60.0).toInt().formatSection()
            "$m:$s"
        }
        else -> {
            "00:$timeSeconds"
        }
    }
}

fun Int.formatSection(): String {
    return if (this < 10) {
        "0$this"
    } else {
        "this"
    }
}